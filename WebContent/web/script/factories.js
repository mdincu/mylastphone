app.factory("PhoneCommentFactory", function() {
	
	var comments = {
	    "Apple":{
	    	"iPhone": "Oooh you had an iPhone... Nice!",
	    	"iPhone 4": "Did you get signal?",
	    	"iPhone 6": "They should call it \"iPhone Bends\""
	    },
	    "Nokia":{
	    	"9210 Communicator": "Communicator... You should be a very rich person",
	    	"5110": "Best Phone Forever!",
	    	"8800": "Do you still have it?",
	    	"7650": "Advanced technology, it had a good camera"
	    },
	    "Motorola":{
	    	"RAZR V3": "That was a very beautiful phone!",
	    	"StarTAC 85": "You got style"
	    },
	    "Bosch":{
	    	"Com 509": "You didn't have money for a Nokia 5110, did you?"
	    }
	};
	
	return{
		getComment: function(phoneBrand, phoneName){
			if(comments[phoneBrand] != undefined && comments[phoneBrand][phoneName] != undefined)
				return (comments[phoneBrand])[phoneName];
			else
				return;
		}
	}
});

app.factory("ResultFactory", function(){

	var displaySizeResult = {
		"small": "We think you have an eagle eye because you love phones that have a small, small screen (like a watch).",
		"medium" : "Your devices have a medium screen, not too big, not too small.",
		"big" : "You chose only phones with a big screen (we hope you're not a girl), we strongly recommend you to buy smaller phones and a pair of glasses!",
		"huge" : "Your last phones have had a huge screen (or only some of them), your next mobile phone should be a TV!"
	};
	var favoritesResult = {
		"low":"Your phones didn't get too much favorites so it seems you buy cheap phones and people are not very interested about them.",
		"medium" : "It seems you like average quality phones that have nothing interesting, you are a normal person (or not?).",
		"high" : "Phones you usually buy are very beautiful and everybody wants to have one of them (PS: they are very expensive)!",
	};
	var weightResult = {
		"light":"Also your phones are very light.",
		"medium" : "You had medium weight phones.",
		"heavy" : "You love heavy phones, you should be a person with strong muscles!"
	};
	
	return{
		getDisplaySizeResult: function(displaySize){
			if(displaySize <= 2) return displaySizeResult["small"];
			else if(displaySize > 2 && displaySize <= 4) return displaySizeResult["medium"];
			else if(displaySize > 4 && displaySize <= 5.5) return displaySizeResult["big"];
			else return displaySizeResult["huge"];
		},
		getFavoriteResult: function(favorites){
			if(favorites <= 15) return favoritesResult["low"];
			else if(favorites > 15 && favorites <= 30)  return favoritesResult["medium"];
			else return favoritesResult["high"];
		},
		getWeightResult: function(weight){
			if(weight <= 100) return weightResult["light"];
			else if(weight > 100 && weight <= 200) return weightResult["medium"];
			else return weightResult["heavy"];
		},
		getOSResult: function(os){
			switch(os){
				case "Android":
					return "Android phones tend to be the most advanced technically, but often the most raw. Most applications are free, but privacy and security aren't really Google's thing, since the phones are partially funded by an advertising model. Android phone users are likely the closest to technology geeks; the phones are best used by guys who like to tinker with things and folks who like a bargain. Android users want the most choice in carriers and phone designs, and don't mind a little aggravation in exchange for having something that is different. Their personal motto is likely \"viva la difference.\"";				
				case "BlackBerry":
					return "The BlackBerry is arguably the best phone (for those who forgot what a phone is supposed to do, it is supposed to easily take and make phone calls) in its class. It does a few things really well, like PDA features, but lacks much depth in applications and is most often purchased by companies. BlackBerry users are buttoned down and work focused. They like feeling secure, and believe that everything has a place, and that it should be in that place. Life isn't about play, it is about making money, and they'd likely prefer wearing a suit to any other form of dress. It doesn't mean they don't have a wild side though, and these folks are most likely to have a second personal phone that isn't a BlackBerry for nonprofessional use. Their professional motto is likely \"time is money.\"";
				case "iOS":
					return "The iPhone is an elegant package with more applications than any other device, tied at the hip to a company that does more to control all aspects of the experience than any other firm. The iPhone user is more status oriented than the others and likely very cliquish. They want to belong, and aren't huge fans of having to muck with technology. They expect things to work, and for vendors to take care of them. They like to play, but they like to show off more. They want to belong, be part of something, and certainly not stand out and be different. They know quality when the see it, and are willing to pay for it. For the Apple fan, their personal motto is likely, \"paaaaaarty!\"";
			}
		}
	}
});

app.factory("RNFactory", function(){
	return{
		getRandom: function(min, max){
			return Math.floor((Math.random()*((max-min)+1))+min);
		}
	}
});