app.controller("MainController", function($rootScope, $scope, $http, $anchorScroll, $location, $timeout, ResultFactory) {
	
	$rootScope.inputs = [{phoneBrand: "", phoneName: ""}];
	$rootScope.details = new Array();
	
	var PhoneObject = function(){
		return{
			phoneBrand: "",
			phoneName: ""
		}
	}
	
	$rootScope.addInput = function(){		
		var list = $rootScope.inputs;
		list.push(new PhoneObject());
	}
	
	$scope.getResult = function(){

		var json = $rootScope.json;
		var linkList = new Array();		
		var inputList = $rootScope.inputs;		
		
		for(var i = 0; i < inputList.length; i++){
			var phone = inputList[i];
			var phoneBrand = phone.phoneBrand;
			if(json[phoneBrand] == undefined) continue;
			var brandPhones = json[phoneBrand].phones;
			var link = JSON.parse(brandPhones)[phone.phoneName].link;
			linkList.push(link);			
		}
					
		$http({
            method: "POST",
            url: "/mylastphone/rest/details/phone_link",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                phone_link: JSON.stringify(linkList)
            }
        })
        .success(function(response){
        	$rootScope.details = response;
		}).then(function(){
			$scope.phonesCount = $rootScope.details.length;			
			var totalDisplaySize = 0;			
			var totalWeight = 0;
			var totalFavorites = 0;
			var totalPopularity = 0;
			var phonesYearRelease = new Array();
			for(var i=0; i<$scope.phonesCount; i++){
				var phone = $rootScope.details[i];
				var phoneDisplaySize = new Number(phone.displaySize);
				var phoneWeight = new Number(phone.phoneWeight);
				var favorites = new Number(phone.favoriteCount);
				var popularity = new Number(phone.popularity);
				if(!isNaN(phoneDisplaySize)){
					totalDisplaySize += phoneDisplaySize;
				}
				if(!isNaN(phoneWeight)){
					totalWeight += phoneWeight;
				}
				if(!isNaN(favorites)){
					totalFavorites += favorites;
				}
				if(!isNaN(popularity)){
					totalPopularity += popularity;
				}
				phonesYearRelease.push(phone.yearRelease);
			}
			phonesYearRelease.sort();
			var phonesPeriod = phonesYearRelease[phonesYearRelease.length-1] - phonesYearRelease[0];
			
			$scope.totalDisplaySize = totalDisplaySize;
			$scope.averageDisplaySize = totalDisplaySize/$scope.phonesCount;
			$scope.totalWeight = totalWeight;
			$scope.averageWeight = totalWeight/$scope.phonesCount;
			$scope.totalFavorites = totalFavorites;
			$scope.averageFavorites = totalFavorites/$scope.phonesCount;
			$scope.popularity = popularity;
			$scope.averagePopularity = (totalPopularity/$scope.phonesCount).toFixed(2);
			$scope.phonesPeriod = phonesPeriod;

			$scope.getDisplayResult = ResultFactory.getDisplaySizeResult($scope.averageDisplaySize);
			$scope.getFavoriteResult = ResultFactory.getFavoriteResult($scope.averageFavorites);
			$scope.getWeightResult = ResultFactory.getWeightResult($scope.averageWeight);
			
			function compare(a,b) {
				  if (new Number(a.favoriteCount) < new Number(b.favoriteCount))
				    return -1;
				  if (new Number(a.favoriteCount) > new Number(b.favoriteCount))
				    return 1;
				  return 0;
				}				
			var copyArrayDetails = JSON.parse(JSON.stringify($rootScope.details));			
			copyArrayDetails.sort(compare);
				
			var bestPhone = copyArrayDetails[copyArrayDetails.length-1];
			$scope.bestPhoneName = bestPhone.name;
			$scope.bestPhoneFavorites = bestPhone.favoriteCount;
			$scope.bestPhoneOS = ResultFactory.getOSResult(bestPhone.os);
			
			$rootScope.fbCaption = "The most beautiful phone I had is " + bestPhone.name + ".";
			$rootScope.fbLink = "http://www.mylastphone.com";
			$rootScope.fbRedirectURI = "http://www.mylastphone.com";
			$rootScope.fbPicture = bestPhone.photoLink;
			
			$rootScope.fbDescription = "It has a total of " + bestPhone.favoriteCount + " favorites. Throughout time I had ";
			$rootScope.fbDescription += ($scope.phonesCount==1) ? "only " : "";
			$rootScope.fbDescription += $scope.phonesCount + " phone";
			$rootScope.fbDescription += ($scope.phonesCount>1) ? "s and starting from my oldest phone to the newest one have been past " + $scope.phonesPeriod + " years." : ".";
						
		});
		
	}
	
	$scope.resetInputs = function(){
		$rootScope.inputs = new Array();
		$timeout(function(){
			$rootScope.inputs.push({phoneBrand: "", phoneName: ""});
		}, 500);
	}
	
});