app.directive("headPanel", function() {
	return {
		restrict : "E",
		templateUrl : "/mylastphone/web/body/head_panel.html?2"
	}
});

app.directive("leftPanel", function() {
	return {
		restrict : "E",
		templateUrl : "/mylastphone/web/body/left_panel.html?2"
	}
});

app.directive("rightPanel", function() {
	return {
		restrict : "E",
		templateUrl : "/mylastphone/web/body/right_panel.html?2"
	}
});

app.directive("footerPanel", function() {
	return {
		restrict : "E",
		templateUrl : "/mylastphone/web/body/footer_panel.html?2"
	}
});

app.directive("messageModal", function(){
	return{
		restrict: "E",
		templateUrl : "/mylastphone/web/body/message_modal.html?2"
	}
});