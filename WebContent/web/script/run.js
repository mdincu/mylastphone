app.run(function($rootScope, $http, $anchorScroll, $location, $timeout, RNFactory){

	//$http.get("/mylastphone/rest/phones/brands") //parse http://www.gsmarena.com	
	$http.get("/mylastphone/web/script/json.txt")	
	.success(function(response) {
		
		$rootScope.json = response;
		
		var getBrands = function(){
			var brands = new Array();
			for(var brand in $rootScope.json){
				brands.push(brand);
			}
			return brands;
		}
		$rootScope.brands = getBrands();	
		
	});
	
	$rootScope.goToBottom = function() {
		$timeout(function(){
			document.getElementById("bottom").scrollIntoView();
		}, 100);
	}
	
	$rootScope.random = RNFactory.getRandom(8,11);
	
});