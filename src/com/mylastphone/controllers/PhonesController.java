package com.mylastphone.controllers;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.mylastphone.dbbeans.Brand;
import com.mylastphone.dbbeans.Phone;
import com.mylastphone.utilities.Brands;

@Path("/phones")
public class PhonesController{
	
	List<Brand> brands;
	
	private PhonesController(){
		brands = Brands.getInstance().getBrands();		
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/brands")
	public String getBrands(){
		
		JSONObject brandsList = new JSONObject();
		
		try{
			
			for(Brand brand: brands){				

				JSONObject phonesList = new JSONObject();				
				for(Phone phone: brand.getPhones()){
					JSONObject phonesObject = new JSONObject();
					phonesObject.put("link", phone.getLink());
					phonesObject.put("photo", phone.getPhoto());
					phonesList.put(phone.getName(), phonesObject);
				}
				
				JSONObject brandsObject = new JSONObject();
				brandsObject.put("link", brand.getLink());
				brandsObject.put("photo", brand.getPhoto());
				brandsObject.put("phones", phonesList.toString());
				brandsList.put(brand.getName(), brandsObject);
				
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return brandsList.toString();
		
	}

}
