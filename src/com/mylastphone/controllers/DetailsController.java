package com.mylastphone.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/details")
public class DetailsController {
	
	@POST
	@Path("/phone_link")
	public Response getDetails(@FormParam("phone_link") String phoneLink){
		
		List<JSONObject> jsonDetails = new ArrayList<>();
		
		try {
			
			JSONArray jsonArray = new JSONArray(phoneLink);
			
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jsonObject = new JSONObject();
				String link = (String) jsonArray.get(i);
				String urlString = "http://m.gsmarena.com/" + link;		
				URL url = new URL(urlString);
				URLConnection urlConnection = url.openConnection();
				BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;		
				while((line = br.readLine()) != null){
					sb.append(line.replace("\t", ""));
				}
				
				String name = getDetailValue(sb.toString(), "<h1 class=\"section nobor\">([.[^<>]]*)</h1>");
				String yearRelease = getDetailValue(sb.toString(), "Announced</a></td><td class=\"nfo\">[.[^<>]]*([\\d]{4})[.[^<>]]*</td>");
				String popularity = getDetailValue(sb.toString(), "<strong>([\\d\\.]+)%</strong><span>Popularity</span>");
				String favoriteCount = getDetailValue(sb.toString(), "<strong>(\\d+)</strong><span>Favorites</span>");
				String phoneHeight = getDetailValue(sb.toString(),"Dimensions</a></td><td class=\"nfo\">([\\d\\.]+)[.[^<>]]*</td>");
				String phoneWidth = getDetailValue(sb.toString(),"Dimensions</a></td><td class=\"nfo\">[\\d\\.]+ x ([\\d\\.]+)[.[^<>]]*</td>");
				String phoneThickness = getDetailValue(sb.toString(),"Dimensions</a></td><td class=\"nfo\">[\\d\\.]+ x [\\d\\.]+ x ([\\d\\.]+)[.[^<>]]*</td>");
				String phoneWeight = getDetailValue(sb.toString(),"Weight</a></td><td class=\"nfo\">([\\d]+)[.[^<>]]*</td>");
				String displaySize = getDetailValue(sb.toString(),"Size</a></td><td class=\"nfo\">([\\d\\.]+)[.[^<>]]*</td>");
				String os = getDetailValue(sb.toString(),"OS</a></td><td class=\"nfo\">([a-zA-z]+)[.[^<>]]*</td>");
		
				jsonObject.put("name", name);
				jsonObject.put("yearRelease", yearRelease);
				jsonObject.put("popularity", popularity);
				jsonObject.put("favoriteCount", favoriteCount);
				jsonObject.put("phoneHeight", phoneHeight);
				jsonObject.put("phoneWidth", phoneWidth);
				jsonObject.put("phoneThickness", phoneThickness);
				jsonObject.put("phoneWeight", phoneWeight);
				jsonObject.put("displaySize", displaySize);
				jsonObject.put("os", os);
				
				jsonDetails.add(jsonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}		
				
		return Response.status(200).entity(jsonDetails.toString()).build();
	}
	
	public String getDetailValue(String source, String regex){
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(source);		
		while(m.find()){
			return m.group(1);
		}
		return null;
	}
	
}
