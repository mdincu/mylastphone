package com.mylastphone.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	public static void main(String[] args) {
		
		String source = "<tr><th rowspan=\"4\" scope=\"row\">Platform</th><td class=\"ttl\"><a href=\"glossary.php3?term=os\">OS</a></td><td class=\"nfo\">iOS 4, upgradable to iOS 8.4</td></tr>";
		String regex ="OS</a></td><td class=\"nfo\">([a-zA-z]+)[.[^<>]]*</td>";
		
		System.out.println(getDetailValue(source, regex));
	}
	
	public static String getDetailValue(String source, String regex){
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(source);		
		while(m.find()){
			return m.group(1);
		}
		return null;
	}
}
