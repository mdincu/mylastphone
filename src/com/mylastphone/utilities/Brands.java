package com.mylastphone.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mylastphone.dbbeans.Brand;
import com.mylastphone.dbbeans.Phone;

public class Brands extends Thread{
	
	List<Brand> brands;
	
	private Brands(){
		
		try {
			this.setBrands();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.start();
		
	}
	
	interface Singleton{
		Brands brands = new Brands();
	}
	
	public static Brands getInstance(){
		return Singleton.brands;
	}
	
	public void setBrands() throws MalformedURLException, IOException{
		
		ArrayList<Brand> brandsList = new ArrayList<>();

		Pattern p = Pattern.compile("<td><a href=([.[^<>]]*)><img src=\"([.[^<>]]*)\" width=92 height=22 border=0 alt=\"([.[^<>]]*)\"></a></td>");
		Matcher m;

		URL url = new URL("http://www.gsmarena.com/makers.php3");
		URLConnection urlConnection = url.openConnection();
		urlConnection.addRequestProperty("User-Agent", "Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		int no = 0;
		String line;
		while ((line = br.readLine()) != null) {
			m = p.matcher(line);
			while (m.find()) {
				
				String name = m.group(3);
				String link = "http://www.gsmarena.com/" + m.group(1);
				String photo = m.group(2);
				List<Phone> phones = new Phones().getPhones(link);
				
				System.out.printf("%d. %s has %d phones%n",++no, name, phones.size());
				
				Brand brand = new Brand(name, link, photo, phones);
				
				brandsList.add(brand);
				
			}
		}
		
		brands = brandsList;
		
	}
	
	public List<Brand> getBrands(){
		return this.brands;
	}
	
	@Override
	public void run(){
		try{
			long updateTime = 1000l * 60 * 60 * 24;
			Thread.sleep(updateTime);
			this.setBrands();
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}catch(MalformedURLException mue){
			mue.printStackTrace();
		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	
}
