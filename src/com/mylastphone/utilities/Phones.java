package com.mylastphone.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mylastphone.dbbeans.Phone;

public class Phones {
	
	public List<Phone> getPhones(String brandLink) throws MalformedURLException, IOException{
		
		ArrayList<Phone> phonesList = new ArrayList<>();
		
		for(String page: getPages(brandLink)){
			URL url = new URL(page);
			URLConnection urlConnection = url.openConnection();
			urlConnection.addRequestProperty("User-Agent", "Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1");
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	
			Pattern p = Pattern.compile("<li><a href=\"([.[^<>]]*)\"><img src=([.[^<>]]*) title=\"[.[^<>]]*\"><strong><span>([.[^<>]]*)</span></strong></a></li>");
			Matcher m;
	
			String line;
			while ((line = br.readLine()) != null) {
				m = p.matcher(line);
				while (m.find()) {
					
					String name = m.group(3);
					String link = m.group(1);
					String photo = m.group(2);				
					Phone phone = new Phone(name, link, photo);
					
					phonesList.add(phone);
					
				}
			}		
		}
		
		return phonesList;
		
	}
	
	public ArrayList<String> getPages(String enterLink) throws MalformedURLException, IOException{
		
		ArrayList<String> pagesList = new ArrayList<>();
		
		pagesList.add(enterLink);
		
		URL url = new URL(enterLink);
		URLConnection urlConnection = url.openConnection();
		urlConnection.addRequestProperty("User-Agent", "Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));	
		
		Pattern p = Pattern.compile("<a class=\"pages-next\" href=\"([.[^<>]]*)\" title=\"Next page\"></a>");
		Matcher m;
		
		String line;
		while ((line = br.readLine()) != null) {
			m = p.matcher(line);
			if (m.find()) {				
				String link = "http://www.gsmarena.com/" + m.group(1);
				pagesList.addAll(getPages(link));
			}
		}
		
		return pagesList;
		
	}
	
}
