package com.mylastphone.dbbeans;

public class Details {
	
	private String popularity;
	private String favoriteCount;
	private String phoneDimensions;
	private String phoneWeight;
	private String displaySize;
	
	public Details(){}
	
	public Details(String popularity, String favoritCount,
			String phoneDimensions, String phoneWeight, String displaySize) {
		this.popularity = popularity;
		this.favoriteCount = favoritCount;
		this.phoneDimensions = phoneDimensions;
		this.phoneWeight = phoneWeight;
		this.displaySize = displaySize;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	public String getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(String favoritCount) {
		this.favoriteCount = favoritCount;
	}

	public String getPhoneDimensions() {
		return phoneDimensions;
	}

	public void setPhoneDimensions(String phoneDimensions) {
		this.phoneDimensions = phoneDimensions;
	}

	public String getPhoneWeight() {
		return phoneWeight;
	}

	public void setPhoneWeight(String phoneWeight) {
		this.phoneWeight = phoneWeight;
	}

	public String getDisplaySize() {
		return displaySize;
	}

	public void setDisplaySize(String displaySize) {
		this.displaySize = displaySize;
	}
	
}
