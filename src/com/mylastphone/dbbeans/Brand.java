package com.mylastphone.dbbeans;

import java.util.List;

public class Brand {
	
	private String name;
	private String link;
	private String photo;
	private List<Phone> phones;
	
	public Brand(String name, String link, String photo, List<Phone> phones) {
		super();
		this.name = name;
		this.link = link;
		this.photo = photo;
		this.phones = phones;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	
}
