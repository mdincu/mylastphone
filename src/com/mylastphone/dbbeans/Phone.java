package com.mylastphone.dbbeans;

public class Phone {
	
	private String name;
	private String link;
	private String photo;
	
	public Phone() {
	}
	
	public Phone(String name, String link, String photo) {
		this.name = name;
		this.link = link;
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
}
